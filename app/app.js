import $ from 'jquery';

import BpmnModeler from 'bpmn-js/lib/Modeler';
import customModule from './custom';

//// Loading the diagram file from the flask API into the diagramXML variable
// Declaring the global variable diagramXML
var diagramXML

// Fetching the data and filling it into diagramXML
$.getJSON('http://35.232.174.253:5000/getinitialdiagram', function(data) {
    // JSON result in `data` variable
    console.log( "successfully loaded the initial diagram containing the solids" );
    var parsedData = JSON.parse(JSON.stringify(data));
    diagramXML = decodeURIComponent(parsedData['initialXmlData'])
    console.log("successfully loaded the diagram into diagramXML")
});
////Loading complete


var container = $('#js-drop-zone');

var modeler = new BpmnModeler({
  container: '#js-canvas',
  additionalModules: [customModule]
});

function createNewDiagram() {
  openDiagram(diagramXML);
}

async function openDiagram(xml) {

  try {

    await modeler.importXML(xml);

    container
      .removeClass('with-error')
      .addClass('with-diagram');
  } catch (err) {

    container
      .removeClass('with-diagram')
      .addClass('with-error');

    container.find('.error pre').text(err.message);

    console.error(err);
  }
}

function registerFileDrop(container, callback) {

  function handleFileSelect(e) {
    e.stopPropagation();
    e.preventDefault();

    var files = e.dataTransfer.files;

    var file = files[0];

    var reader = new FileReader();

    reader.onload = function(e) {

      var xml = e.target.result;

      callback(xml);
    };

    reader.readAsText(file);
  }

  function handleDragOver(e) {
    e.stopPropagation();
    e.preventDefault();

    e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  }

  container.get(0).addEventListener('dragover', handleDragOver, false);
  container.get(0).addEventListener('drop', handleFileSelect, false);
}


// file drag / drop ///////////////////////

// check file api availability
if (!window.FileList || !window.FileReader) {
  window.alert(
    'Looks like you use an older browser that does not support drag and drop. ' +
    'Try using Chrome, Firefox or the Internet Explorer > 10.');
} else {
  registerFileDrop(container, openDiagram);
}

// bootstrap diagram functions

$(function() {

  $('#js-create-diagram').click(function(e) {
    e.stopPropagation();
    e.preventDefault();

    createNewDiagram();
  });

  var executeLink = $('#js-execute-diagram');
  var downloadLink = $('#js-download-diagram');
  var downloadSvgLink = $('#js-download-svg');

  $('.buttons a').click(function(e) {
    if (!$(this).is('.active')) {
      e.preventDefault();
      e.stopPropagation();
    }
  });

  function setEncoded(link, name, data) {
    var encodedData = encodeURIComponent(data);

    //If data is present
    if (data) {

      // Default link
      link.addClass('active').attr({
        'href': 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData
      });

      // Overwriting link if the function is an execution function
      if (link[0]['id'] == "js-execute-diagram") {
        link[0]['href'] = 'http://35.232.174.253:5000/executedagster?xmlString='+encodedData
      }

    } else {
      //If no data is present
      link.removeClass('active');
    }
  }

  var exportArtifacts = debounce(async function() {

    try {

      const { svg } = await modeler.saveSVG();

      setEncoded(downloadSvgLink, 'diagram.svg', svg);
    } catch (err) {

      console.error('Error happened saving svg: ', err);
      setEncoded(downloadSvgLink, 'diagram.svg', null);
    }

    try {

      const { xml } = await modeler.saveXML({ format: true });
      setEncoded(downloadLink, 'diagram.bpmn', xml);
    } catch (err) {

      console.error('Error happened saving XML: ', err);
      setEncoded(downloadLink, 'diagram.bpmn', null);
    }

    try {

      const { xml } = await modeler.saveXML({ format: true });
      setEncoded(executeLink, 'diagram.bpmn', xml);
    } catch (err) {

      console.error('Error happened executing XML: ', err);
      setEncoded(executeLink, 'diagram.bpmn', null);
    }

  }, 500);

  modeler.on('commandStack.changed', exportArtifacts);
});



// helpers //////////////////////

function debounce(fn, timeout) {

  var timer;

  return function() {
    if (timer) {
      clearTimeout(timer);
    }

    timer = setTimeout(fn, timeout);
  };
}
